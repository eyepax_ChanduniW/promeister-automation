**Basic guideline to setup Promeister Automation project **

Please note that this document includes installation steps for new cypress project as well. You do not need to install cypress again after cloning this cypress project.


---

## Installing Cypress for a new project

SYSTEM REQUIREMENTS 

Windows 7 and above

PREREQUISITES

1. Check whether Node.js is already installed using following command. 
**node -v**

	If not,

Download and install Node.js from https://nodejs.org/en/download/

INSTALLING CYPRESS

1. Check again for node version using **node -v** and npm version using **npm -v**.

2. Create a folder for the new project and run **npm init** command using command prompt in the root of your project.

3. Run **npm install cypress --save-dev** command to install cypress in the project folder.

4. Download and install VS Code from https://code.visualstudio.com/downloadand and open the project. 



## Cloning a cypress project

When cloning a cypress project no need to install cypress again. Make sure that you have downloaded and installed Node.js as per the above instructions.

1. Clone the project from https://bitbucket.org/eyepax_ChanduniW/promeister-automation/src/master/ using SourceTree.
 
2. Create a new local branch and get a pull from master.

3. Before you move on, go ahead and explore the repository. Checkout the **Source**, **Commits**, **Branches**, and **Settings** pages.

4. Download and install VS Code from https://code.visualstudio.com/downloadand and open the project.


---

## References

Please find the link to Cypress documentation below

https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements



---

## Extra guide to clone a repository

Use these steps to clone from SourceTree, Bitbucket client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.

2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.

3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.

4. Open the directory you just created to see your repository’s files.

