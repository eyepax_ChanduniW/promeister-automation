import loginPageSelectors from '../support/LoginPageSelectors.js';
import chooseSitePageLocators from '../support/ChooseSitePageLocators.js';
import courseCataloguePageSelectors from '../support/CourseCataloguePageSelectors.js';
import menuLocators from '../support/MenuLocators.js';
import courseCalendarPageLocators from '../support/CourseCalendarPageLocators.js';
import courseInfoPageLocators from '../support/CourseInfoPageLocators.js';
import settingsPageLocators from '../support/SettingsPageLocators.js';
import scheduledCoursePageLocators from '../support/ScheduledCoursesPageLocators.js';
import scheduledCourseInfoPageLocators from '../support/ScheduledCourseInfoPageLocators.js';
import transactionsPageLocators from '../support/TransactionsPageLocators.js';
import skillOverviewPageLocators from '../support/SkillOverviewPageLocators';
import testsPageLocators from '../support/TestsPageLocators.js';
import testDetailsPageLocators from '../support/TestDetailsPageLocators.js';
import groupsPageLocators from '../support/GroupsPageLocators.js';
import companiesPageLocators from '../support/CompaniesPageLocators.js';


describe('Settings Scenarios', function () {

  beforeEach(function () {
    cy.fixture('data').then(function (data) {
      this.data = data
      cy.visit('/')
    })
  })

  it('Verify membership discount ', function () {

    //login 
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.get(courseCataloguePageSelectors.PageTitle).should('contain', 'Course catalogue')
    cy.get(menuLocators.SettingsButton).click()
    cy.get(settingsPageLocators.SettingsList).contains('Companies').click()
    cy.get(groupsPageLocators.PageTitle).should('contain', 'Companies')
    cy.get(companiesPageLocators.SearchBox).type('Test Eyepax Services 25').type('{enter}')
    cy.wait(1000)
    cy.xpath(companiesPageLocators.CompanyLink).click()
    cy.xpath(companiesPageLocators.DiscountSection).then(($Discount) => {
      const DiscountTrimmed = ($Discount.text()).trim()
      const Discount = (DiscountTrimmed).split("%")[0]
      cy.get(menuLocators.SettingsButton).click()
      cy.get(settingsPageLocators.SettingsList).contains('Groups').click()
      cy.get(groupsPageLocators.PageTitle).should('contain', 'Groups')
      cy.get(groupsPageLocators.GroupLink).contains(this.data.group).click()
      cy.xpath(groupsPageLocators.PriceBookLink).click()
      cy.wait(3000)
      cy.xpath(groupsPageLocators.TestPriceColumn).then(($TestPrice) => {
        const TestPriceTrimmed = ($TestPrice.text()).trim()
        const TestPrice = (TestPriceTrimmed).split(" ")[0]
        const FinalPriceFromSettings = ((100 - Discount) / 100) * TestPrice
        cy.get(menuLocators.SkillOverviewButton).click()
        cy.get(skillOverviewPageLocators.ActiveBreadcrumb).should('contain', this.data.userName)
        cy.wait(10000)
        cy.get(skillOverviewPageLocators.RelevantTestLink).contains(this.data.test).click()
        cy.xpath(skillOverviewPageLocators.TestPriceLabel).then(($testPriceInTheTest) => {
          const testPriceWithoutCurrency = ($testPriceInTheTest.text()).trim()
          const testPriceActual = testPriceWithoutCurrency.substr(0, testPriceWithoutCurrency.indexOf(' '));
          expect(FinalPriceFromSettings.toFixed(2)).to.eq(testPriceActual)

        })
      })
    })
  })
})

