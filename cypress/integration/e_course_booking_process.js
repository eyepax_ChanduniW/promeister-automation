import loginPageSelectors from '../support/LoginPageSelectors.js';
import chooseSitePageLocators from '../support/ChooseSitePageLocators.js';
import courseCataloguePageSelectors from '../support/CourseCataloguePageSelectors.js';
import menuLocators from '../support/MenuLocators.js';
import courseCalendarPageLocators from '../support/CourseCalendarPageLocators.js';
import courseInfoPageLocators from '../support/CourseInfoPageLocators.js';
import settingsPageLocators from '../support/SettingsPageLocators.js';
import scheduledCoursePageLocators from '../support/ScheduledCoursesPageLocators.js';
import scheduledCourseInfoPageLocators from '../support/ScheduledCourseInfoPageLocators.js';
import notificationListPageLocators from '../support/NotificationListPageLocators.js';
import transactionsPageLocators from '../support/TransactionsPageLocators.js';
import eCoursePageLocators from '../support/ECoursePageLocators.js';


describe('Booking process for E-course', function () {

  before(function () {
    cy.fixture('data').then(function (data) {
      this.data = data
      cy.visit('/')


    })


  })
  it('Booking is made, cancelled and deleted successfully', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')
    const now = Cypress.moment()
    cy.log("================date&Time " + now)
    cy.log("===================format1 " + now.format("hh:mm:ss K")) // 1:00:00 PM
    cy.log("====================format2 " + now.format("HH:mm:ss"))
    const nowClone1 = now.clone()
    const nowClone2 = now.clone()
    const nowClone3 = now.clone()
    const nowClone4 = now.clone()

    //Login to the site
    cy.log("=======================UserName" + this.data.email)
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    //Select LMS
    cy.wait(500)
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.get(courseCataloguePageSelectors.PageTitle).should('contain', 'Course catalogue')
    //Book a course
    cy.get(menuLocators.ECoursesButton).click()
    cy.get(eCoursePageLocators.PageTitle).should('contain', 'E-course')
    cy.get(eCoursePageLocators.SkillTextFilter).type(this.data.skill)
    cy.get(eCoursePageLocators.FilteredSkill).click()
    cy.get(eCoursePageLocators.SearchButton).click()
    cy.get(eCoursePageLocators.SkillCardAfterFiltering).click()
    cy.get(eCoursePageLocators.EcourseLink).contains(this.data.ecourseName).click()
    cy.xpath('//h5[contains(text(),"Price")]/../../div/h3').then(($price) => {
      const price = ($price.text().trim()).split(" ")[0]
      cy.log("==============================price " + price)
      cy.get(eCoursePageLocators.ConfirmPriceButton).click()





    })

  })
})

