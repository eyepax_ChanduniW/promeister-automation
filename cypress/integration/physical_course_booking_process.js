import loginPageSelectors from '../support/LoginPageSelectors.js';
import chooseSitePageLocators from '../support/ChooseSitePageLocators.js';
import courseCataloguePageSelectors from '../support/CourseCataloguePageSelectors.js';
import menuLocators from '../support/MenuLocators.js';
import courseCalendarPageLocators from '../support/CourseCalendarPageLocators.js';
import courseInfoPageLocators from '../support/CourseInfoPageLocators.js';
import settingsPageLocators from '../support/SettingsPageLocators.js';
import scheduledCoursePageLocators from '../support/ScheduledCoursesPageLocators.js';
import scheduledCourseInfoPageLocators from '../support/ScheduledCourseInfoPageLocators.js';
import notificationListPageLocators from '../support/NotificationListPageLocators.js';
import transactionsPageLocators from '../support/TransactionsPageLocators.js';


describe('Booking process for physical courses through course calendar', function () {

  before(function () {
    cy.fixture('data').then(function (data) {
      this.data = data
      cy.visit('/')
    })
  })

  it('Booking is made, cancelled and deleted successfully', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')
    const now = Cypress.moment()
    const nowClone1 = now.clone()
    const nowClone2 = now.clone()
    const nowClone3 = now.clone()
    const nowClone4 = now.clone()

    //Login to the site
    cy.log("=======================UserName" + this.data.email)
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    //Select LMS
    cy.wait(500)
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.get(courseCataloguePageSelectors.PageTitle).should('contain', 'Course catalogue')
    //Book a course
    cy.get(menuLocators.CourseCalendarButton).click({ force: true })
    cy.get(courseCalendarPageLocators.PageTitle).should('contain', 'Course calendar')
    cy.get(courseCalendarPageLocators.CourseLink).contains(this.data.courseName).click()
    cy.get(courseInfoPageLocators.BookButton).click({ force: true })
    cy.wait(1000)
    cy.get(courseInfoPageLocators.ConfirmButton).click({ force: true })
    cy.get(courseInfoPageLocators.SuccessAlert).should('contain', 'Congratulations you are eligible for this course')
    cy.get(courseInfoPageLocators.ConfirmPriceButton).click({ force: true })
    let courseRequestServerTime = nowClone1.add(8, 'minutes').format("HH:mm")
    let ServerTimePlus1 = nowClone3.add(9, 'minutes').format("HH:mm")
    let ServerTimeMinus1 = nowClone4.add(7, 'minutes').format("HH:mm")
    cy.wait(3000)
    cy.reload()
    cy.get(courseInfoPageLocators.SuccessAlert).should('contain', 'You have already registered to the course')

    //Verify Notifications 
    cy.get(menuLocators.NotificationListButton).click()
    cy.get(notificationListPageLocators.PageTitle).should('contain', 'Notification')

    cy.xpath(notificationListPageLocators.MessageTypeColumnFirstRow).then(($messageType1) => {
      const messageType1 = ($messageType1.text()).trim()
      expect(messageType1).to.eq("Course signup")

      cy.xpath(notificationListPageLocators.MessageTypeColumnSecondRow).then(($messageType2) => {
        const messageType2 = ($messageType2.text()).trim()
        expect(messageType2).to.eq("Course signup")

        cy.xpath(notificationListPageLocators.MessageTypeColumnThirdRow).then(($messageType3) => {
          const messageType3 = ($messageType3.text()).trim()
          expect(messageType3).to.eq("Course request approved")

          cy.xpath(notificationListPageLocators.MessageTypeColumnFourthRow).then(($messageType4) => {
            const messageType4 = ($messageType4.text()).trim()
            expect(messageType4).to.eq("Course request approved")

            cy.xpath(notificationListPageLocators.TransactionDateColumnFirstRow).then(($TransactionDateAndTime1) => {
              const transactionDateAndTime1 = ($TransactionDateAndTime1.text()).trim()
              const transactionDate1 = (transactionDateAndTime1).split(" ")[0]
              const transactionTime1 = (transactionDateAndTime1).split(" ")[1]

              expect(transactionTime1).to.be.oneOf([courseRequestServerTime, ServerTimePlus1, ServerTimeMinus1])
              expect(transactionDate1).to.eq(todaysDate)

              cy.xpath(notificationListPageLocators.TransactionDateColumnSecondRow).then(($TransactionDateAndTime2) => {
                const transactionDateAndTime2 = ($TransactionDateAndTime2.text()).trim()
                const transactionDate2 = (transactionDateAndTime1).split(" ")[0]
                const transactionTime2 = (transactionDateAndTime2).split(" ")[1]
                expect(transactionTime2).to.be.oneOf([courseRequestServerTime, ServerTimePlus1, ServerTimeMinus1])
                expect(transactionDate2).to.eq(todaysDate)

                cy.xpath(notificationListPageLocators.TransactionDateColumnThirdRow).then(($TransactionDateAndTime3) => {
                  const transactionDateAndTime3 = ($TransactionDateAndTime3.text()).trim()
                  const transactionDate3 = (transactionDateAndTime1).split(" ")[0]
                  const transactionTime3 = (transactionDateAndTime3).split(" ")[1]
                  expect(transactionTime3).to.be.oneOf([courseRequestServerTime, ServerTimePlus1, ServerTimeMinus1])
                  expect(transactionDate3).to.eq(todaysDate)

                  cy.xpath(notificationListPageLocators.TransactionDateColumnFourthRow).then(($TransactionDateAndTime4) => {
                    const transactionDateAndTime4 = ($TransactionDateAndTime4.text()).trim()
                    const transactionTime4 = (transactionDateAndTime4).split(" ")[1]
                    const transactionDate4 = (transactionDateAndTime1).split(" ")[0]
                    expect(transactionTime4).to.be.oneOf([courseRequestServerTime, ServerTimePlus1, ServerTimeMinus1])
                    expect(transactionDate4).to.eq(todaysDate)
                  })
                })
              })
            })
          })
        })
      })
    })
    cy.get(menuLocators.SettingsButton).click()
    cy.wait(1000)
    //Cancel and delete
    cy.get(settingsPageLocators.SettingsList).contains('Scheduled course').click({ force: true })
    cy.get(scheduledCoursePageLocators.PageTitle).should('contain', 'Scheduled courses')
    cy.get(scheduledCoursePageLocators.CourseLink).contains(this.data.courseName).click({ force: true })
    cy.wait(2000)
    cy.get(scheduledCourseInfoPageLocators.UpdateStatusIcon).click({ force: true })
    cy.wait(1000)
    cy.get(scheduledCourseInfoPageLocators.RadioButton).find(scheduledCourseInfoPageLocators.CancelOption).click({ force: true });
    cy.get(scheduledCourseInfoPageLocators.UpdateStatusActionButton).contains('Save').click()
    cy.wait(500)
    cy.reload()
    cy.get(scheduledCourseInfoPageLocators.CancelledStatus).should('be.visible')
    cy.get(menuLocators.SettingsButton).click()
    cy.wait(1000)
    cy.get(settingsPageLocators.SettingsList).contains('Scheduled course').click()
    cy.get(scheduledCoursePageLocators.CourseLink).contains(this.data.courseName).click({ force: true })
    cy.get(scheduledCourseInfoPageLocators.DeleteIcon).click({ force: true })
    cy.wait(500)
    cy.get(scheduledCourseInfoPageLocators.DeletePopupButton).contains('Delete').click()
    cy.get(scheduledCourseInfoPageLocators.UserNameLink).contains(this.data.userName).should('not.visible')





  })
})

