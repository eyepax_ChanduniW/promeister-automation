import loginPageSelectors from '../support/LoginPageSelectors.js';
import chooseSitePageLocators from '../support/ChooseSitePageLocators.js';
import courseCataloguePageSelectors from '../support/CourseCataloguePageSelectors.js';
import menuLocators from '../support/MenuLocators.js';
import courseCalendarPageLocators from '../support/CourseCalendarPageLocators.js';
import courseInfoPageLocators from '../support/CourseInfoPageLocators.js';
import settingsPageLocators from '../support/SettingsPageLocators.js';
import scheduledCoursePageLocators from '../support/ScheduledCoursesPageLocators.js';
import scheduledCourseInfoPageLocators from '../support/ScheduledCourseInfoPageLocators.js';
import transactionsPageLocators from '../support/TransactionsPageLocators.js';
import notificationListPageLocators from '../support/NotificationListPageLocators.js';

describe('Ready for Invoice process for physical courses through course calendar', function () {

  before(function () {
    cy.fixture('data').then(function (data) {
      this.data = data
      cy.visit('/')
    })
  })

  it('Booking is made, ready for invoice successfully', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')
    const now = Cypress.moment()
    const nowClone1 = now.clone()
    const nowClone2 = now.clone()
    const nowClone3 = now.clone()
    const nowClone4 = now.clone()
    cy.log("================date&Time " + now)
    cy.log("===================format1 " + now.format("hh:mm:ss K")) 
    cy.log("====================format2 " + now.format("HH:mm"))
    
    //Login to the site
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()

    //Select LMS
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.get(courseCataloguePageSelectors.PageTitle).should('contain', 'Course catalogue')
    //Book a course
    cy.get(menuLocators.CourseCalendarButton).click()
    cy.get(courseCalendarPageLocators.PageTitle).should('contain', 'Course calendar')
    cy.get(courseCalendarPageLocators.CourseLink).contains(this.data.courseName).click()

    //get course price before confirming course
    cy.xpath('//h5[contains(text(),"Price")]/../h3').then(($price) => {
      const price = ($price.text()).split(" ")[0]
      cy.get(courseInfoPageLocators.BookButton).click()
      cy.wait(1000)
      cy.get(courseInfoPageLocators.ConfirmButton).click()
      cy.get(courseInfoPageLocators.SuccessAlert).should('contain', 'Congratulations you are eligible for this course')
      cy.get(courseInfoPageLocators.ConfirmPriceButton).click()
      cy.wait(3000)
      cy.reload()
      cy.get(courseInfoPageLocators.SuccessAlert).should('contain', 'You have already registered to the course')
      cy.get(menuLocators.SettingsButton).click()
      cy.wait(1000)
      cy.get(settingsPageLocators.SettingsList).contains('Scheduled course').click()
      cy.get(scheduledCoursePageLocators.PageTitle).should('contain', 'Scheduled courses')

      //Ready for Invoice
      cy.get(scheduledCoursePageLocators.CourseLink).contains(this.data.courseName).click()
      cy.wait(2000)
      cy.get(scheduledCourseInfoPageLocators.SelectParticipantCheckbox).click({ force: true })
      cy.get(scheduledCourseInfoPageLocators.ReadyForInvoicingLinkToButton).contains('Ready for invoicing').click()
      cy.get(scheduledCourseInfoPageLocators.ReadyToInvoiceSaveButton).click()
      // Get server time when doing ready for invoice
      let readyForInvoiceServerTime = nowClone1.add(8, 'minutes').format("HH:mm")
      let ServerTimePlus1 = nowClone3.add(9, 'minutes').format("HH:mm")
      let ServerTimeMinus1 = nowClone4.add(7, 'minutes').format("HH:mm")
      cy.reload()
      cy.get(scheduledCourseInfoPageLocators.ReadyForInvoicingBadge).should('contain', 'Ready for invoicing ')
      cy.xpath(scheduledCourseInfoPageLocators.ReadyForInvoicingDate).should('contain', todaysDate)

      // verify price in transactions
      cy.get(menuLocators.TransactionListButton).click()
      cy.get(transactionsPageLocators.PageTitle).should('contain', 'Transactions')
      cy.xpath(transactionsPageLocators.PriceColumnFirstRow).then(($priceInTransactions) => {
        const priceInTransactions = $priceInTransactions.text()
        cy.log(priceInTransactions)
        expect(price).to.eq(priceInTransactions)
        cy.xpath(transactionsPageLocators.TransactionDateColumnFirstRow).then(($TransactionDateAndTime) => {
          const transactionDateAndTime = ($TransactionDateAndTime.text()).trim()
          const transactionDate = (transactionDateAndTime).split(" ")[0]
          const transactionTime = (transactionDateAndTime).split(" ")[1]
          expect(transactionTime).to.be.oneOf([readyForInvoiceServerTime, ServerTimePlus1, ServerTimeMinus1])
          expect(transactionDate).to.eq(todaysDate)
        })
      })
    });

    //Cancel 
    cy.get(menuLocators.SettingsButton).click()
    cy.wait(1000)
    cy.get(settingsPageLocators.SettingsList).contains('Scheduled course').click()
    cy.get(scheduledCoursePageLocators.PageTitle).should('contain', 'Scheduled courses')
    cy.get(scheduledCoursePageLocators.CourseLink).contains(this.data.courseName).click()
    cy.get(scheduledCourseInfoPageLocators.UpdateStatusIcon).click()
    cy.wait(1000)
    cy.get(scheduledCourseInfoPageLocators.RadioButton).find(scheduledCourseInfoPageLocators.CancelOption).click({ force: true });
    cy.get(scheduledCourseInfoPageLocators.UpdateStatusActionButton).contains('Save').click()
    let cancelCourseServerTime = nowClone2.add(7, 'minutes').format("HH:mm")
    cy.log("==================cancelCourseServerTime " + cancelCourseServerTime)
    cy.wait(500)
    cy.reload()
    cy.get(scheduledCourseInfoPageLocators.CancelledStatus).should('be.visible')

    //go to notification page and verify cancel notification
    cy.get(menuLocators.NotificationListButton).click()
    cy.get(notificationListPageLocators.PageTitle).should('contain', 'Notification')

    cy.xpath(notificationListPageLocators.MessageTypeColumnFirstRow).then(($messageType) => {
      const messageType = ($messageType.text()).trim()
      expect(messageType).to.eq("Remove from course")
    })
    
    //Delete
    cy.get(menuLocators.SettingsButton).click()
    cy.wait(1000)
    cy.get(settingsPageLocators.SettingsList).contains('Scheduled course').click()
    cy.get(scheduledCoursePageLocators.CourseLink).contains(this.data.courseName).click()
    cy.get(scheduledCourseInfoPageLocators.DeleteIcon).click()
    cy.wait(500)
    cy.get(scheduledCourseInfoPageLocators.DeletePopupButton).contains('Delete').click()
    cy.get(scheduledCourseInfoPageLocators.UserNameLink).contains(this.data.userName).should('not.visible')

  })
})