import loginPageSelectors from '../support/LoginPageSelectors.js';
import chooseSitePageLocators from '../support/ChooseSitePageLocators.js';
import courseCataloguePageSelectors from '../support/CourseCataloguePageSelectors.js';
import menuLocators from '../support/MenuLocators.js';
import courseCalendarPageLocators from '../support/CourseCalendarPageLocators.js';
import courseInfoPageLocators from '../support/CourseInfoPageLocators.js';
import settingsPageLocators from '../support/SettingsPageLocators.js';
import scheduledCoursePageLocators from '../support/ScheduledCoursesPageLocators.js';
import scheduledCourseInfoPageLocators from '../support/ScheduledCourseInfoPageLocators.js';
import transactionsPageLocators from '../support/TransactionsPageLocators.js';
import skillOverviewPageLocators from '../support/SkillOverviewPageLocators';
import testsPageLocators from '../support/TestsPageLocators.js';
import testDetailsPageLocators from '../support/TestDetailsPageLocators.js';
import groupsPageLocators from '../support/GroupsPageLocators.js';


describe('Test booking process for cost approval having user', function () {

  beforeEach(function () {
    cy.fixture('data').then(function (data) {
      this.data = data
      cy.visit('/')
    })
  })

  it.skip('Test booking is made, attempted successfully', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')
    const now = Cypress.moment()
    const nowClone1 = now.clone()
    const nowClone2 = now.clone()
    const nowClone3 = now.clone()
    const nowClone4 = now.clone()
    cy.log("================date&Time " + now)
    cy.log("===================format1 " + now.format("hh:mm:ss K")) // 1:00:00 PM
    cy.log("====================format2 " + now.format("HH:mm"))
    var price

    //login 
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })

    //Go to skill overview page
    cy.get(menuLocators.SkillOverviewButton).click()
    cy.get(skillOverviewPageLocators.ActiveBreadcrumb).should('contain', this.data.userName)
    cy.wait(10000)
    cy.get(skillOverviewPageLocators.RelevantTestLink).contains(this.data.test).click()

    //Get the test price 
    cy.xpath(skillOverviewPageLocators.TestPriceLabel).then(($testPrice) => {
      const testPrice = ($testPrice.text()).trim()
      const testPriceWithoutCurrency = testPrice.substr(0, testPrice.indexOf(' '));

      cy.get(skillOverviewPageLocators.TestPriceOkButton).click()
      //Get time when confirming test
      let testConfirmServerTime = nowClone1.add(8, 'minutes').format("HH:mm")
      let ServerTimePlus1 = nowClone3.add(9, 'minutes').format("HH:mm")
      let ServerTimeMinus1 = nowClone4.add(7, 'minutes').format("HH:mm")

      //Do the test
      cy.wait(5000)
      cy.get(skillOverviewPageLocators.StartTestButton).click({ force: true })
      cy.wait(4000)
      cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
      cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
      cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
      cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
      cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
      cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
      cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
      cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
      cy.get(skillOverviewPageLocators.QuizSubmitButton).click({ force: true })
      cy.xpath(skillOverviewPageLocators.BackToOverviewButton).click()

      //Verify price in Transactions
      cy.get(menuLocators.TransactionListButton).click()
      cy.get(transactionsPageLocators.PageTitle).should('contain', 'Transactions')
      cy.xpath(transactionsPageLocators.PriceColumnFirstRow).then(($priceInTransactions) => {
        const priceInTransactions = $priceInTransactions.text()
        expect(testPriceWithoutCurrency).to.eq(priceInTransactions)

        //Verify date and time in transactions
        cy.xpath(transactionsPageLocators.TransactionDateColumnFirstRow).then(($TransactionDateAndTime) => {
          const transactionDateAndTime = ($TransactionDateAndTime.text()).trim()
          const transactionDate = (transactionDateAndTime).split(" ")[0]
          const transactionTime = (transactionDateAndTime).split(" ")[1]
          expect(transactionTime).to.be.oneOf([testConfirmServerTime, ServerTimePlus1, ServerTimeMinus1])
          expect(transactionDate).to.eq(todaysDate)
        })

      })

    })

  })
  it('Test booking is redone successfully', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')

    //login 
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })

    //Go to skill overview page
    cy.get(menuLocators.SkillOverviewButton).click()
    cy.get(skillOverviewPageLocators.ActiveBreadcrumb).should('contain', this.data.userName)
    cy.wait(10000)
    cy.get(skillOverviewPageLocators.RelevantTestLink).contains(this.data.test).click()
    cy.get(skillOverviewPageLocators.TestPriceOkButton).click({ force: true })
    cy.wait(5000)

    //Do the test
    cy.get(skillOverviewPageLocators.StartTestButton).click({ force: true })
    cy.wait(4000)
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizSubmitButton).click({ force: true })
    cy.xpath(skillOverviewPageLocators.BackToOverviewButton).click()
    cy.get(skillOverviewPageLocators.PrimaryButton).contains('Redo test').click()
    cy.wait(2000)

    //Re do the test
    cy.get(skillOverviewPageLocators.TestPriceOkButton).click({ force: true })
    cy.wait(5000)
    cy.get(skillOverviewPageLocators.StartTestButton).click({ force: true })
    cy.wait(4000)
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
    cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
    cy.get(skillOverviewPageLocators.QuizSubmitButton).click({ force: true })
    cy.xpath(skillOverviewPageLocators.BackToOverviewButton).click()


  })

  it('Edit test questions and verify test passed', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')

    //login 
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    //cy.get('a[href*="questions"]').click()
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.wait(1000)

    //Edit Questions
    //cy.get(menuLocators.SettingsButton).click()
    //  cy.get(settingsPageLocators.SettingsList).contains('Tests').click()
    //  cy.get(testsPageLocators.PageTitle).should('contain','Tests')
    //  cy.get(testsPageLocators.TestSearchField).type(this.data.test)
    //  cy.get(testsPageLocators.PrimaryButton).contains('Search').click()
    //  cy.get(testsPageLocators.TestLink).contains(this.data.test).click()
    //  cy.get(testDetailsPageLocators.ActiveBreadcrumb).should('contain','Demo Test')
    //  cy.get(testDetailsPageLocators.AssignedQuestionLinks).first().click()
    //  cy.get(testDetailsPageLocators.ActiveBreadcrumb).should('contain',this.data.question1AsInTheBreadcrumb)
    //  cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click({ force: true })
    //  cy.wait(2000)
    //  cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //  cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question1Edited)
    //  cy.get(testDetailsPageLocators.PointsTextField).clear().type("10")
    //  cy.get(testDetailsPageLocators.LevelDropdown).select('Medium')
    //  cy.xpath(testDetailsPageLocators.Question1Answer1Checkbox).click({ force: true })
    //  cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })
    //  cy.get(testDetailsPageLocators.QuestionName).should('contain',this.data.question1Edited)
    //  cy.xpath(testDetailsPageLocators.Points).should('contain',"10")
    //  cy.xpath(testDetailsPageLocators.Level).should('contain','Medium')
    //  cy.get(testDetailsPageLocators.AssignedTestsLinks).contains(this.data.test).click({ force: true })
    //  cy.get(testDetailsPageLocators.AssignedQuestionLinks).eq(1).click({ force: true })
    //  cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click({ force: true })
    //  cy.wait(2000)
    //   cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question2)
    //   cy.get(testDetailsPageLocators.PointsTextField).clear().type("5")
    //   cy.get(testDetailsPageLocators.MinusPointsTextField).clear().type("1")
    //   cy.get(testDetailsPageLocators.LevelDropdown).select('Medium')
    //   cy.xpath(testDetailsPageLocators.Question2Answer1Checkbox).click({ force: true })
    //   cy.xpath(testDetailsPageLocators.Question2Answer2Checkbox).click({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })
    //   cy.get(testDetailsPageLocators.AssignedTestsLinks).contains(this.data.test).click({ force: true })
    //   cy.get(testDetailsPageLocators.AssignedQuestionLinks).eq(2).click({ force: true })
    //   cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click({ force: true })
    //   cy.wait(2000)
    //   cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question3)
    //   cy.get(testDetailsPageLocators.PointsTextField).clear().type("5")
    //   cy.get(testDetailsPageLocators.MinusPointsTextField).clear().type("2")
    //   cy.get(testDetailsPageLocators.LevelDropdown).select('Hard')
    //   cy.xpath(testDetailsPageLocators.Question3Answer1Checkbox).click({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })
    //   cy.get(testDetailsPageLocators.AssignedTestsLinks).contains(this.data.test).click({ force: true })
    //   cy.get(testDetailsPageLocators.AssignedQuestionLinks).eq(3).click({ force: true })
    //   cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click({ force: true })
    //   cy.wait(2000)
    //   cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question4)
    //   cy.get(testDetailsPageLocators.PointsTextField).clear().type("5")
    //   cy.get(testDetailsPageLocators.MinusPointsTextField).clear().type("3")
    //   cy.xpath(testDetailsPageLocators.Question4Answer1Checkbox).click({ force: true })
    //   cy.get(testDetailsPageLocators.LevelDropdown).select('Hard')
    //   cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })


    //Do the test 

    cy.get(menuLocators.SkillOverviewButton).click()
    cy.get(skillOverviewPageLocators.ActiveBreadcrumb).should('contain', this.data.userName)
    cy.wait(10000)
    cy.xpath(skillOverviewPageLocators.TestScore).then(($testScore) => {
      debugger;
      // const testPrice = ($testPrice.text()).split(" ")[0]
      const testScoreWithPercentage = $testScore.text()
      const testScore = testScoreWithPercentage.replace('%', '').trim()
      const targetScore = "55"
      if (parseInt(testScore) >= parseInt(targetScore)) {
        cy.get(skillOverviewPageLocators.PreviouslyPassedTestLink).contains(this.data.test).click()
      } else {
        cy.get(skillOverviewPageLocators.FailedOrNotAttemptedTestLink).contains(this.data.test).click()
      }

    })

    cy.xpath(skillOverviewPageLocators.TestPriceLabel).then(($testPrice) => {
      const testPrice = ($testPrice.text()).trim()
      const testPriceWithoutCurrency = testPrice.substr(0, testPrice.indexOf(' '));
      cy.get(skillOverviewPageLocators.TestPriceOkButton).click({ force: true })
      cy.wait(5000)
      cy.get(skillOverviewPageLocators.StartTestButton).click({ force: true })
      cy.wait(4000)
      var genArr = Array.from({ length: 4 }, (v, k) => k + 1)
      cy.wrap(genArr).each(() => {
        cy.get(skillOverviewPageLocators.QuestionTitle).then(($title) => {
          const title = $title.text()
          const testQuestion1 = this.data.question1Edited
          const testQuestion2 = this.data.question2
          const testQuestion3 = this.data.question3
          const testQuestion4 = this.data.question4
          if (title == testQuestion1) {
            cy.xpath(skillOverviewPageLocators.DemoQuestion1Answer1).click({ force: true })
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          } else if (title == testQuestion2) {
            cy.xpath(skillOverviewPageLocators.DemoQuestion2Answer1).click({ force: true })
            cy.xpath(skillOverviewPageLocators.DemoQuestion2Answer2).click({ force: true })
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          } else if (title == testQuestion3) {
            cy.xpath(skillOverviewPageLocators.DemoQuestion3Answer1).click({ force: true })
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          } else {
            cy.xpath(skillOverviewPageLocators.DemoQuestion4Answer1).click({ force: true })
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          }
        })
      })
      cy.wait(3000)
      cy.get(skillOverviewPageLocators.QuizSubmitButton).click({ force: true })
      cy.wait(1000)
      cy.get('.prm-card-body div+ h5').then(($actualMarks) => {

        //Verify actual marks against expected
        let actualMarksNoTEdited = $actualMarks.text().trim()
        let actualMarks = actualMarksNoTEdited.split(" ").pop()
        expect(actualMarks).to.eq(this.data.expectedMarksTestPass)
        cy.xpath(skillOverviewPageLocators.BackToOverviewButton).click()

        //Verify test marks in skill overview page
        cy.get(menuLocators.SkillOverviewButton).click()
        cy.wait(7000)
        cy.xpath(skillOverviewPageLocators.TestScore).then(($testScore) => {
          const testScoreWithPercentage = $testScore.text()
          expect(actualMarks).to.eq(testScoreWithPercentage.trim())
          cy.get(skillOverviewPageLocators.TestSuccessButton).should('be.visible')

        })
      })
    })
  })

  it('Edit test questions and verify test failed', function () {
    const todaysDate = Cypress.moment().format('DD-MM-YYYY')

    //login 
    cy.get(loginPageSelectors.UserNameField).type(this.data.email)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.wait(1000)

    //Edit Questions
    //cy.get(menuLocators.SettingsButton).click()
    //  cy.get(settingsPageLocators.SettingsList).contains('Tests').click()
    //  cy.get(testsPageLocators.PageTitle).should('contain','Tests')
    //  cy.get(testsPageLocators.TestSearchField).type(this.data.test)
    //  cy.get(testsPageLocators.PrimaryButton).contains('Search').click()
    //  cy.get(testsPageLocators.TestLink).contains(this.data.test).click()
    //  cy.get(testDetailsPageLocators.ActiveBreadcrumb).should('contain','Demo Test')
    //  cy.get(testDetailsPageLocators.AssignedQuestionLinks).first().click()
    //  cy.get(testDetailsPageLocators.ActiveBreadcrumb).should('contain',this.data.question1AsInTheBreadcrumb)
    //  cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click()
    //  cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //  cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question1Edited)
    //  cy.get(testDetailsPageLocators.PointsTextField).clear().type("10")
    //  cy.get(testDetailsPageLocators.LevelDropdown).select('Medium')
    //  cy.xpath(testDetailsPageLocators.Question1Answer1Checkbox).click({ force: true })
    //  cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })
    //  cy.get(testDetailsPageLocators.QuestionName).should('contain',this.data.question1Edited)
    //  cy.xpath(testDetailsPageLocators.Points).should('contain',"10")
    //  cy.xpath(testDetailsPageLocators.Level).should('contain','Medium')
    //  cy.get(testDetailsPageLocators.AssignedTestsLinks).contains(this.data.test).click()
    //  cy.get(testDetailsPageLocators.AssignedQuestionLinks).eq(1).click()
    //  cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click()
    //   cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question2)
    //   cy.get(testDetailsPageLocators.PointsTextField).clear().type("5")
    //   cy.get(testDetailsPageLocators.MinusPointsTextField).clear().type("1")
    //   cy.get(testDetailsPageLocators.LevelDropdown).select('Medium')
    //   cy.xpath(testDetailsPageLocators.Question2Answer1Checkbox).click({ force: true })
    //   cy.xpath(testDetailsPageLocators.Question2Answer2Checkbox).click({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })
    //   cy.get(testDetailsPageLocators.AssignedTestsLinks).contains(this.data.test).click()
    //   cy.get(testDetailsPageLocators.AssignedQuestionLinks).eq(2).click()
    //   cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click()
    //   cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question3)
    //   cy.get(testDetailsPageLocators.PointsTextField).clear().type("5")
    //   cy.get(testDetailsPageLocators.MinusPointsTextField).clear().type("2")
    //   cy.get(testDetailsPageLocators.LevelDropdown).select('Hard')
    //   cy.xpath(testDetailsPageLocators.Question3Answer1Checkbox).click({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })
    //   cy.get(testDetailsPageLocators.AssignedTestsLinks).contains(this.data.test).click()
    //   cy.get(testDetailsPageLocators.AssignedQuestionLinks).eq(3).click()
    //   cy.get(testDetailsPageLocators.PrimaryButton).contains('Edit').click()
    //   cy.get(testDetailsPageLocators.Checkboxes).uncheck({ force: true })
    //   cy.get(testDetailsPageLocators.QuestionTextField).clear().type(this.data.question4)
    //   cy.get(testDetailsPageLocators.PointsTextField).clear().type("5")
    //   cy.get(testDetailsPageLocators.MinusPointsTextField).clear().type("3")
    //   cy.xpath(testDetailsPageLocators.Question4Answer1Checkbox).click({ force: true })
    //   cy.get(testDetailsPageLocators.LevelDropdown).select('Hard')
    //   cy.get(testDetailsPageLocators.QuestionSaveButton).click({ force: true })


    //Do the test
    cy.get(menuLocators.SkillOverviewButton).click()
    cy.get(skillOverviewPageLocators.ActiveBreadcrumb).should('contain', this.data.userName)
    cy.wait(10000)

    cy.xpath(skillOverviewPageLocators.TestScore).then(($testScore) => {
      debugger;
      // const testPrice = ($testPrice.text()).split(" ")[0]
      const testScoreWithPercentage = $testScore.text()
      const testScore = testScoreWithPercentage.replace('%', '').trim()
      const targetScore = "55"
      if (parseInt(testScore) >= parseInt(targetScore)) {
        cy.get(skillOverviewPageLocators.PreviouslyPassedTestLink).contains(this.data.test).click()
      } else {
        cy.get(skillOverviewPageLocators.FailedOrNotAttemptedTestLink).contains(this.data.test).click()
      }
    })

    cy.xpath(skillOverviewPageLocators.TestPriceLabel).then(($testPrice) => {
      const testPrice = ($testPrice.text()).trim()
      const testPriceWithoutCurrency = testPrice.substr(0, testPrice.indexOf(' '));
      cy.get(skillOverviewPageLocators.TestPriceOkButton).click({ force: true })
      cy.wait(5000)
      cy.get(skillOverviewPageLocators.StartTestButton).click({ force: true })
      cy.wait(4000)
      var genArr = Array.from({ length: 4 }, (v, k) => k + 1)
      cy.wrap(genArr).each(() => {
        cy.get(skillOverviewPageLocators.QuestionTitle).then(($title) => {
          const title = $title.text()
          const testQuestion1 = this.data.question1Edited
          const testQuestion2 = this.data.question2
          const testQuestion3 = this.data.question3
          const testQuestion4 = this.data.question4

          if (title == testQuestion1) {
            cy.xpath(skillOverviewPageLocators.DemoQuestion1Answer1).click({ force: true })
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          } else if (title == testQuestion2) {
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          } else if (title == testQuestion3) {
            cy.xpath(skillOverviewPageLocators.DemoQuestion3Answer1).click({ force: true })
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          } else {
            cy.get(skillOverviewPageLocators.PrimaryButton).contains('Next').click({ force: true })
            cy.wait(500)
          }
        })
      })
      cy.wait(3000)
      cy.get(skillOverviewPageLocators.QuizSubmitButton).click({ force: true })
      cy.wait(1000)
      cy.get('.prm-card-body div+ h5').then(($actualMarks) => {

        //Verify actual marks against expected
        let actualMarksNoTEdited = $actualMarks.text().trim()
        let actualMarks = actualMarksNoTEdited.split(" ").pop()
        cy.log("=================ActualMarkTrimmed " + actualMarksNoTEdited)
        expect(actualMarks).to.eq(this.data.expectedMarksTestFail)
        cy.xpath(skillOverviewPageLocators.BackToOverviewButton).click()

        //Verify test marks in skill overview page
        cy.get(menuLocators.SkillOverviewButton).click()
        cy.wait(7000)
        cy.xpath(skillOverviewPageLocators.TestScore).then(($testScore) => {

          // const testPrice = ($testPrice.text()).split(" ")[0]
          const testScoreWithPercentage = $testScore.text()
          expect(actualMarks).to.eq(testScoreWithPercentage.trim())

        })
      })
    })
  })

  it('Test registration with membership discount and verify redeemed count', function () {

    //login 
    cy.get(loginPageSelectors.UserNameField).type(this.data.unlimitedUserEmail)
    cy.get(loginPageSelectors.NextButton).click()
    cy.get(loginPageSelectors.PasswordField).type(this.data.password)
    cy.get(loginPageSelectors.SubmitButton).click()
    cy.xpath(chooseSitePageLocators.SiteButton).click({ force: true })
    cy.get(courseCataloguePageSelectors.PageTitle).should('contain', 'Course catalogue')
    cy.get(menuLocators.SettingsButton).click()
    cy.get(settingsPageLocators.SettingsList).contains('Groups').click()
    cy.get(groupsPageLocators.PageTitle).should('contain', 'Groups')
    cy.get(groupsPageLocators.GroupLink).contains(this.data.group).click()
    cy.xpath(groupsPageLocators.PriceBookLink).click({ force: true })
    cy.wait(3000)
    cy.xpath(groupsPageLocators.TestPriceColumn).then(($TestPrice) => {
      const TestPriceTrimmed = ($TestPrice.text()).trim()
      const TestPrice = (TestPriceTrimmed).split(" ")[0]
      cy.log("=============TestPrice " + TestPrice)

      cy.get(menuLocators.MembershipImage).click()
      cy.wait(2000)
      cy.get(menuLocators.MembershipDetailForTest).then(($MembershipDetail) => {
        const MembershipDetail = ($MembershipDetail.text()).trim()
        const MembershipDiscount = (MembershipDetail).split("%")[0]
        cy.log("===================MenmershipDiscount " + MembershipDiscount)


        cy.get(menuLocators.RedeemedCountForTest).then(($CurrentRedeemedCount) => {
          const CurrentRedeemedCount = ($CurrentRedeemedCount.text()).trim()
          cy.log("===============CurrentRedeemedCount " + CurrentRedeemedCount)

          cy.xpath(menuLocators.MembershipPopupCloseButton).click()
          const FinalPriceFromSettings = ((100 - MembershipDiscount) / 100) * TestPrice
          cy.log("========================FinalPriceFromSettings " + FinalPriceFromSettings)

          cy.get(menuLocators.SkillOverviewButton).click()
          cy.get(skillOverviewPageLocators.ActiveBreadcrumb).should('contain', this.data.unlimitedUserName)
          cy.wait(10000)
          cy.get(skillOverviewPageLocators.RelevantTestLink).contains(this.data.test).click()
          cy.xpath(skillOverviewPageLocators.TestPriceLabel).then(($testPriceInTheTest) => {
            const testPrice = ($testPriceInTheTest.text()).split(" ")[0]
            const testPriceWithoutCurrency = ($testPriceInTheTest.text()).trim()
            const testPriceActual = testPriceWithoutCurrency.substr(0, testPriceWithoutCurrency.indexOf(' '));
            expect(FinalPriceFromSettings.toFixed(2)).to.eq(testPriceActual)


            //DO the test
            cy.get(skillOverviewPageLocators.TestPriceOkButton).click({ force: true })

            cy.wait(5000)
            cy.get(skillOverviewPageLocators.StartTestButton).click({ force: true })
            cy.wait(4000)
            cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
            cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
            cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
            cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
            cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
            cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
            cy.get(skillOverviewPageLocators.QuizChoice).first().click({ force: true })
            cy.xpath(skillOverviewPageLocators.QuizNextButton).click({ force: true })
            cy.get(skillOverviewPageLocators.QuizSubmitButton).click({ force: true })
            cy.xpath(skillOverviewPageLocators.BackToOverviewButton).click()
            cy.wait(2000)
            cy.get(menuLocators.MembershipImage).click({ force: true })
            cy.wait(2000)
            cy.get(menuLocators.RedeemedCountForTest).then(($UpdatedRedeemedCount) => {
              const UpdatedRedeemedCount = ($UpdatedRedeemedCount.text()).trim()
              cy.log("=====================UpdatedRedeemedCount " + UpdatedRedeemedCount)
              expect(UpdatedRedeemedCount - CurrentRedeemedCount).to.eq(1)

            })
          })
        })
      })
    })
  })
})