export default {
    PageTitle: '.dashhead-title',
    SentDateColumnFirstRow: '//table//tbody//tr[1]/td[9]',
    SentDateColumnSecondRow: '//table//tbody//tr[2]/td[9]',
    MessageTypeColumnFirstRow: '//table//tbody//tr[1]/td[5]',
    MessageTypeColumnSecondRow: '//table//tbody//tr[2]/td[5]',
    MessageTypeColumnThirdRow: '//table//tbody//tr[3]/td[5]',
    MessageTypeColumnFourthRow: '//table//tbody//tr[4]/td[5]',
    TransactionDateColumnFirstRow: '//table//tbody//tr[1]/td[9]',
    TransactionDateColumnSecondRow: '//table//tbody//tr[2]/td[9]',
    TransactionDateColumnThirdRow: '//table//tbody//tr[3]/td[9]',
    TransactionDateColumnFourthRow: '//table//tbody//tr[4]/td[9]'
    
  };