export default {
    PageTitle: '.dashhead-title',
    TestSearchField: 'input[name="searchText"]',
    PrimaryButton: '.btn-primary',
    GroupLink: 'table > tbody > tr > td > a',
    PriceBookLink: '(//a[contains(@href,"pricebook")])[1]',
    TestPriceColumn: '//h5[@id="test-heading"]/../../../..//table/tbody/tr/td[contains(text(),"Demo Test")]/../td[4]',
    

  };