export default {
    UpdateStatusIcon: '#all > div.prm-table-responsive-wrap.main-table > table > tbody > tr > td:nth-child(7) > table > tbody > tr > td:nth-child(5) > a > span',
    RadioButton: '.custom-radio',
    CancelOption: '#booking-cancel',
    UpdateStatusActionButton: 'div > a',
    CancelledStatus: '.cancelled_participant_1',
    DeleteIcon: '#all > div.prm-table-responsive-wrap.main-table > table > tbody > tr > td:nth-child(7) > table > tbody > tr > td:nth-child(2) > a > span',
    DeletePopupButton: '.modal-footer > .btn-danger',
    UserNameLink: 'p > a',
    SelectParticipantCheckbox: 'input[class=mark_selected_participants]',
    ReadyForInvoicingLinkToButton: 'a',
    ReadyToInvoiceSaveButton: '#markBtn',
    ReadyForInvoicingBadge: 'td > span.badge-primary',
    ReadyForInvoicingDate: '//li//h5[contains(text(),"Ready for invoicing")]/../p/span/span[1]'
    

  };