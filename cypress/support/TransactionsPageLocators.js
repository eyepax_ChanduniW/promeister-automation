export default {
    PageTitle: '.dashhead-title',
    PriceColumnFirstRow: '//table//tbody//tr[1]/td[11]',
    TransactionDateColumnFirstRow: '//table//tbody//tr[1]/td[5]',
    TransactionDateColumnSecondRow: '//table//tbody//tr[2]/td[5]',
    TransactionDateColumnThirdRow: '//table//tbody//tr[3]/td[5]',
    TransactionDateColumnFourthRow: '//table//tbody//tr[4]/td[5]'
    

  };