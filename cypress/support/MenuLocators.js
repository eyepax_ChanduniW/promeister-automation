export default {
    CourseCalendarButton: 'a[title="Course calendar"]',
    SettingsButton: 'a[title="Settings"]',
    TransactionListButton: 'a[title="Transaction list"]',
    SkillOverviewButton: 'a[title="Skill overview"]',
    NotificationListButton: 'a[title="Notification list"]',
    ECoursesButton: 'a[title="E-courses"]',
    MembershipImage: '#membership-img',
    MembershipDetailForTest: '#membership-detail-test',
    RedeemedCountForTest: '#membership-detail-test-redeemed',
    MembershipPopupCloseButton: '//h5[contains(text(),"Membership Details")]/../button',
    

  };