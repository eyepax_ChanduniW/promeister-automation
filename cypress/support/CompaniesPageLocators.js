export default {
    PageTitle: '.dashhead-title',
    SearchBox: '#searchInput',
    CompanyLink: '//table//tbody//tr/td/a[contains(text(),"Test Eyepax Services 25")]',
    DiscountSection: '//li//h5[contains(text(),"Discount")]/../span',

  };