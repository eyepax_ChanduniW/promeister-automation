export default {
    // PageTitle: '.dashhead-title',
    ActiveBreadcrumb: '.breadcrumb-item.active',
    AssignedQuestionLinks: 'table > tbody >tr >td > a',
    QuestionTextField: '#internal_note',
    PointsTextField: '#point',
    MinusPointsTextField: '#minus_point',
    LevelDropdown: '#level',
    Question1Answer1Checkbox: '//span[contains(text(),"are generally applied on structural steel")]/../..//div/div/div/label/input',
    Question1Answer2Checkbox: '//span[contains(text(),"are less durable as compared to enamel paints")]/../..//div/div/div/label/input',
    Question2Answer1Checkbox: '//span[contains(text(),"Quartz sand")]/../..//div/div/div/label/input',
    Question2Answer2Checkbox: '//span[contains(text(),"Pure gypsum")]/../..//div/div/div/label/input',
    Question3Answer1Checkbox: '//span[contains(text(),"Di-calcium silicate")]/../..//div/div/div/label/input',
    Question4Answer1Checkbox: '//span[contains(text(),"Test answer 1")]/../..//div/div/div/label/input',
    PrimaryButton: 'a.btn-primary',
    Checkboxes: '[type="checkbox"]',
    QuestionSaveButton: 'button[id=submit-question-btn]',
    QuestionName: '.prm-card-body >p',
    Points: '//ul/li/h5[contains(text(),"Points")]/../span',
    Level: '//ul/li/h5[contains(text(),"Level")]/../span',
    AssignedTestsLinks: 'td > a',
    
    
    
 
   };