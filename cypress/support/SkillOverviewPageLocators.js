export default {
   // PageTitle: '.dashhead-title',
   ActiveBreadcrumb: '.breadcrumb-item.active',
   TestSuccessButton: '.prm-rounded-icon.prm-bg-success',
   RelevantTestLink: '.relevant-test-div > div > div >#dashboard-relevant-tests-riot >div >table >tbody > tr >td >span >a',
   FailedOrNotAttemptedTestLink: '#dashboard-relevant-tests-riot table > tbody > tr >td > span >a',
   PreviouslyPassedTestLink: '#dashboard-relevant-tests-riot table > tbody > tr >td >table >tbody >tr >td >a',
   TestScore: '//h5[contains(text(),"Relevant tests")]/../../div/table/tbody/tr/td[2]/div',
   TestPriceLabel: '//h5[contains(text(),"Price")]/../../div/h3',
   TestPriceOkButton: '#prmConfirmStartTestBtn',
   StartTestButton: '#startTest',
   QuizChoice: '.answerChoice',
   QuizNextButton: '//a[contains(text(),"  Next ")]',
   PrimaryButton: '.btn-primary',
   QuestionTitle: '.col-md-12 > div > div > h5',
   DemoQuestion1Answer1: '//ul//li/div/span[contains(text(),"are generally applied on structural steel")]/../div/label/input',
   DemoQuestion2Answer1: '//ul//li/div/span[contains(text(),"Quartz sand")]/../div/label/input',
   DemoQuestion2Answer2: '//ul//li/div/span[contains(text(),"Pure gypsum")]/../div/label/input',
   DemoQuestion3Answer1: "//ul//li/div/span[contains(text(),'Di-calcium silicate')]/../div/label/input",
   DemoQuestion4Answer1: '//ul//li/div/span[contains(text(),"Test answer 1")]/../div/label/input',
   QuizSubmitButton: '#prm-quiz-submit',
   BackToOverviewButton: '//a[contains(text(),"  Back to overview ")]',

   

  };